#include "binarys.hpp"
using namespace density;

template<class Type>
Type objective_function<Type>::operator() ()
{
  // Radial Velocity data of the primary
  DATA_INTEGER(doVr);
  DATA_VECTOR(Vr);
  DATA_VECTOR(sigVr);
  DATA_VECTOR(timeVr);
  DATA_VECTOR(VrEpoch); // in case different shifts in RV for different epochs.
  // Radial velocity data of the secondary
  DATA_INTEGER(doVr2);
  DATA_VECTOR(Vr2);
  DATA_VECTOR(sigVr2);
  DATA_VECTOR(timeVr2);
  DATA_VECTOR(VrEpoch2); // in case different shifts in RV for different epochs.
  // Direct Imaging data
  DATA_INTEGER(doDI);
  DATA_VECTOR(ksi21);
  DATA_VECTOR(eta21);
  DATA_VECTOR(sigksi21);
  DATA_VECTOR(sigeta21);
  DATA_VECTOR(corksieta21);
  DATA_VECTOR(timeDi);
  // Hipparcos astrometric data
  DATA_INTEGER(doIAD);
  DATA_VECTOR(Ares);
  DATA_VECTOR(Asig);
  DATA_VECTOR(dpra_nu);
  DATA_VECTOR(dpdec_nu);
  DATA_VECTOR(dppi_nu);
  DATA_VECTOR(dpmura_nu);
  DATA_VECTOR(dpmudec_nu);
  DATA_VECTOR(epoch);
  DATA_SCALAR(Sra);
  DATA_SCALAR(Sdec);
  DATA_SCALAR(Splx);
  DATA_SCALAR(Smualpha);
  DATA_SCALAR(Smudelta);
  // Transit Data HIP1
  DATA_INTEGER(doTD1);
  DATA_SCALAR(TD_K);
  DATA_SCALAR(TD_M1);
  DATA_SCALAR(TD_M2);
  DATA_VECTOR(b1);
  DATA_VECTOR(sb1);
  DATA_VECTOR(b2);
  DATA_VECTOR(sb2);
  DATA_VECTOR(b3);
  DATA_VECTOR(sb3);
  DATA_VECTOR(b4);
  DATA_VECTOR(sb4);
  DATA_VECTOR(b5);
  DATA_VECTOR(sb5);
  DATA_VECTOR(fx);
  DATA_VECTOR(fy);
  DATA_VECTOR(fp);
  // Transit Data HIP2
  DATA_INTEGER(doTD2);
  DATA_INTEGER(doSigFactor);
  DATA_VECTOR(HacHdc);
  DATA_VECTOR(sHacHdc);
  DATA_VECTOR(Beta4);
  DATA_VECTOR(sBeta4);
  DATA_VECTOR(Beta5);
  DATA_VECTOR(sBeta5);
  // Adjust Variability
  DATA_INTEGER(doVAR);
  DATA_VECTOR(Hp);
  DATA_INTEGER(meanHp);
  // Gaia astrometric data
  DATA_INTEGER(doGDR);
  DATA_INTEGER(solGAP);
  DATA_VECTOR(GDRAP);
  DATA_VECTOR(GDRAPsig);
  DATA_MATRIX(GDRcormat);
  DATA_VECTOR(Gfake_time);
  DATA_VECTOR(Gfake_dpra_nu);
  DATA_VECTOR(Gfake_dpdec_nu);
  DATA_VECTOR(Gfake_dppi_nu);
  DATA_VECTOR(Gfake_dpmura_nu);
  DATA_VECTOR(Gfake_dpmudec_nu);
  DATA_SCALAR(GaiaEpoch);
  // Gaia B: resolved star
  DATA_INTEGER(solGAPB);
  DATA_VECTOR(GDRAPB);
  DATA_MATRIX(GDRcormatB);
  DATA_VECTOR(GDRAPsigB);
  // Hipparcos parallax zero point
  DATA_SCALAR(HipPlxZeroPt);
  // dH data
  DATA_INTEGER(dodH);
  DATA_SCALAR(dH);
  DATA_SCALAR(sigdH);
  // Chose set of data to adjust
  DATA_INTEGER(parset);
  // Parameters
  PARAMETER(a1);
  PARAMETER(periodYrs);
  PARAMETER(relTP);
  PARAMETER(ecc);
  PARAMETER(omega2);
  PARAMETER(inclination);
  PARAMETER(nodeangle);
  PARAMETER(par1); // parset=1: M1 - parset=2: a21
  PARAMETER(Vr0);
  PARAMETER(VrEpochShift); // 2/1
  PARAMETER(VrEpochShift2); // 3/1
  PARAMETER(VrEpochShift3); // 4/1
  PARAMETER(VrVarJitter);
  // Astrometry 5P versus Hipparcos solution
  PARAMETER(dra);
  PARAMETER(ddec);
  PARAMETER(dplx);
  PARAMETER(dpmra);
  PARAMETER(dpmdec);
  PARAMETER(beta);
  PARAMETER(A1);

  vector<Type> relposDI;
  vector<Type> DIresiduals(2);
  vector<Type> relposVR;
  vector<Type> relposVR2;
  vector<Type> relposIAD;
  vector<Type> relposTD;
  vector<Type> relposGaia;
  vector<Type> Gfake_Ares(Gfake_time.size());
  vector<Type> Gfake_AresA(Gfake_time.size());
  vector<Type> Gfake_AresB(Gfake_time.size());

  Type r;
  Type r0 = beta/(1-beta); // = A2/A1
  Type betat;
  Type Vrref;
  Type massRatio;
  if (parset==1) {
    Type M1 = par1;
    massRatio = getqfroma1PM1(a1,periodYrs,M1);
  } else {
    Type a21 = par1;
    massRatio = 1/(a21/a1-1);
  }
  Type period = periodYrs*JY2D;
  Type a2 = a1/massRatio;
  Type timeperiastron = relTP * period;
  Type Ashift;
  Type B = massRatio / (1+massRatio); // M2/(M1+M2)
  Type A2 = A1*beta/(1-beta);
  Type p1, p2, b1Sim, b2Sim, b3Sim, b4Sim, b5Sim;
  Type APplx = Splx + dplx;
  Type SSplx = APplx - HipPlxZeroPt;

  Type nll = 0;

  // Direct Imaging
  if (doDI==1) {
    for(int i=0; i<timeDi.size();i++) {
      relposDI = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,timeDi[i]);
      matrix<Type> DIcov(2,2);
      DIcov(0,0) = 1;
      DIcov(0,1) = corksieta21[i];
      DIcov(1,0) = corksieta21[i];
      DIcov(1,1) = 1;
      MVNORM_t<Type> neglogdmvnorm(DIcov);
      DIresiduals[0] = (ksi21[i]-(relposDI[4]-relposDI[0])*SSplx)/sigksi21[i];
      DIresiduals[1] = (eta21[i]-(relposDI[5]-relposDI[1])*SSplx)/sigeta21[i];
      nll += neglogdmvnorm(DIresiduals);
    }
  }
  // Radial Velocity
  if (doVr==1) {
    for(int i=0; i<timeVr.size();i++) {
      relposVR = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,timeVr[i]);
      if (VrEpoch[i]==0 | VrEpoch[i]==1) {
        Vrref = Vr0;
      }
      if (VrEpoch[i]==2) {
        Vrref = Vr0+VrEpochShift;
      }
      if (VrEpoch[i]==3) {
        Vrref = Vr0+VrEpochShift2;
      }
      if (VrEpoch[i]==4) {
        Vrref = Vr0+VrEpochShift3;
      }
      nll -= dnorm(Vr[i],relposVR[3]+Vrref,sqrt(sigVr[i]*sigVr[i]+VrVarJitter),true);
    }
  }
  if (doVr2==1) {
    for(int i=0; i<timeVr2.size();i++) {
      relposVR2 = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,timeVr2[i]);
      Vrref = Vr0;
      if (VrEpoch[i]==2) {
        Vrref = Vrref+VrEpochShift;
      }
      if (VrEpoch[i]==3) {
        Vrref = Vrref+VrEpochShift2;
      }
      if (VrEpoch[i]==4) {
        Vrref = Vrref+VrEpochShift3;
      }
      nll -= dnorm(Vr2[i],relposVR2[7]+Vrref,sigVr2[i],true);
    }
  }
  // Hipparcos
  if (doIAD==1 | doTD2==1) {
    for(int i=0; i<Ares.size();i++) {
      if (doVAR==0) {
        r = r0;
      }
      if (doVAR==1) {
        r = pow( ((1+r0)/r0)*pow(10,-Type(0.4)*(Hp[i]-meanHp))-1 , -1.);
      }
      if (doVAR==2) {
        r = (1+r0)*pow(10,-Type(0.4)*(Hp[i]-meanHp)) - 1.;
      }
      betat = r/(1+r);
      relposIAD = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,(epoch[i]+HIP_EPOCH-2000)*JY2D);
      Type deltaKsi = (relposIAD[4]-relposIAD[0])*SSplx;
      Type deltaEta = (relposIAD[5]-relposIAD[1])*SSplx;
      Type rhoP = dpra_nu[i]*deltaKsi+dpdec_nu[i]*deltaEta;
      Type ksi = 2*M_PI*modulo1(rhoP/HIP_MODGRIDPERIOD);
      Type u = betat*sin(ksi)/(1 - betat + betat*cos(ksi));
      Type phi = 2.*atan(u/(1 + sqrt(1+u*u))); // = atan2...
      Ashift = phi*HIP_MODGRIDPERIOD/(2*M_PI) - B*rhoP;
      Type newAres = Ashift+dpra_nu[i]*dra+dpdec_nu[i]*ddec+dppi_nu[i]*dplx+dpmura_nu[i]*dpmra+dpmudec_nu[i]*dpmdec;
      if (doSigFactor==1) {
        Type sigFactor = (1+r)/sqrt(1+2*r*cos(ksi)+r*r); // Eq. 4.29 and 2.47
        nll -= dnorm(Ares[i], newAres, Asig[i]*sigFactor, true);
      }
      if (doSigFactor==0){
        nll -= dnorm(Ares[i], newAres, Asig[i], true);
      }
      if (doTD2==1) {
        Type nf = pow(1+2*r*cos(ksi)+r*r,3./2.);
        Type HacHdcSim = -2.5*log10(sqrt(1+2*r*cos(ksi)+r*r)/(1+r));
        Type beta4Sim = (1+(r+r*r)*(2*cos(ksi)+cos(2*ksi))+r*r*r)/nf;
        Type beta5Sim = (r-r*r)*(2*sin(ksi)-sin(2*ksi))/nf;
        nll -= dnorm(HacHdc[i],HacHdcSim,sHacHdc[i],true);
        nll -= dnorm(Beta4[i],beta4Sim,sBeta4[i],true);
        nll -= dnorm(Beta5[i],beta5Sim,sBeta5[i],true);
      }
    }
  }
  // TD1
  if (doTD1==1){
    for(int i=0; i<fx.size();i++) {
      relposTD = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,(epoch[i]+HIP_EPOCH-2000)*JY2D);
      p1 = (fx[i]*(dra + relposTD[0]*SSplx + epoch[i]*dpmra) + fy[i]*(ddec + relposTD[1]*SSplx + epoch[i]*dpmdec) + fp[i]*dplx)*MAS2RAD;
      p2 = (fx[i]*(dra + relposTD[4]*SSplx + epoch[i]*dpmra) + fy[i]*(ddec + relposTD[5]*SSplx + epoch[i]*dpmdec) + fp[i]*dplx)*MAS2RAD;
      b1Sim = A1 + A2;
      b2Sim = TD_M1 * (A1*cos(p1) + A2*cos(p2));
      b3Sim = -TD_M1 * (A1*sin(p1)+ A2*sin(p2));
      b4Sim = TD_M2 * (A1*cos(2.*p1) + A2*cos(2.*p2));
      b5Sim = -TD_M2 * (A1*sin(2.*p1) + A2*sin(2.*p2));
      nll -= dnorm(b1[i],b1Sim,sb1[i],true);
      nll -= dnorm(b2[i],b2Sim,sb2[i],true);
      nll -= dnorm(b3[i],b3Sim,sb3[i],true);
      nll -= dnorm(b4[i],b4Sim,sb4[i],true);
      nll -= dnorm(b5[i],b5Sim,sb5[i],true);
    }
  }
  // Magnitude difference dH
  if (dodH==1) {
    nll -= dnorm(dH,log10(1/beta-1)/0.4,sigdH,true);
  }
  // Gaia
  if (doGDR==1) {
    for(int i=0; i<Gfake_time.size(); i++) {
      relposGaia = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,Gfake_time[i]);
      Gfake_Ares[i] = Gfake_dpra_nu[i]*(beta-B)*(relposGaia[4]-relposGaia[0])*SSplx+Gfake_dpdec_nu[i]*(beta-B)*(relposGaia[5]-relposGaia[1])*SSplx;
    }
    vector<Type> gaiares = getIADSolution(Gfake_Ares, Gfake_dpra_nu, Gfake_dpdec_nu, Gfake_dppi_nu, Gfake_dpmura_nu, Gfake_dpmudec_nu);
    vector<Type> gaiatrueAP = epochpropaFull((dra*MAS2DEG)/cos(Sdec*DEG2RAD)+Sra,ddec*MAS2DEG+Sdec,APplx,dpmra+Smualpha,dpmdec+Smudelta,Vr0,HIP_EPOCH,GaiaEpoch);
    vector<Type> gaiaresidualAP(5);
    gaiaresidualAP[0] = ((gaiatrueAP[0]-GDRAP[0])*cos(gaiatrueAP[1]*DEG2RAD)/MAS2DEG + gaiares[0])/GDRAPsig[0];
    gaiaresidualAP[1] = ((gaiatrueAP[1]-GDRAP[1])/MAS2DEG + gaiares[1])/GDRAPsig[1];
    gaiaresidualAP[2] = (gaiatrueAP[2]-GDRAP[2] + gaiares[2])/GDRAPsig[2];
    gaiaresidualAP[3] = (gaiatrueAP[3]-GDRAP[3] + gaiares[3])/GDRAPsig[3];
    gaiaresidualAP[4] = (gaiatrueAP[4]-GDRAP[4] + gaiares[4])/GDRAPsig[4];
    if (solGAP==2) {
      vector<Type> gaiaresidualRADEC(2);
      gaiaresidualRADEC[0] = gaiaresidualAP[0];
      gaiaresidualRADEC[1] = gaiaresidualAP[1];
      matrix<Type> RADECcormat(2,2);
      RADECcormat(0,0) = 1;
      RADECcormat(0,1) = GDRcormat(0,1);
      RADECcormat(1,0) = GDRcormat(1,0);
      RADECcormat(1,1) = 1;
      density::MVNORM_t<Type> GDR2neglogdmvnormRADEC(RADECcormat);
      nll += GDR2neglogdmvnormRADEC(gaiaresidualRADEC);
    } else {
      density::MVNORM_t<Type> GDR2neglogdmvnorm(GDRcormat);
      nll += GDR2neglogdmvnorm(gaiaresidualAP);
    }
  }

  if (doGDR==2) {
    for(int i=0; i<Gfake_time.size(); i++) {
      relposGaia = relpos(period,timeperiastron,ecc,omega2,inclination,nodeangle,a2,massRatio,Gfake_time[i]);
      Gfake_AresA[i] = Gfake_dpra_nu[i]*relposGaia[0]*SSplx+Gfake_dpdec_nu[i]*relposGaia[1]*SSplx; //a1
      Gfake_AresB[i] = Gfake_dpra_nu[i]*relposGaia[4]*SSplx+Gfake_dpdec_nu[i]*relposGaia[5]*SSplx; //a2
    }
    vector<Type> gaiaresA = getIADSolution(Gfake_AresA, Gfake_dpra_nu, Gfake_dpdec_nu, Gfake_dppi_nu, Gfake_dpmura_nu, Gfake_dpmudec_nu);
    vector<Type> gaiaresB = getIADSolution(Gfake_AresB, Gfake_dpra_nu, Gfake_dpdec_nu, Gfake_dppi_nu, Gfake_dpmura_nu, Gfake_dpmudec_nu);
    vector<Type> gaiatrueAP = epochpropaFull((dra*MAS2DEG)/cos(Sdec*DEG2RAD)+Sra,ddec*MAS2DEG+Sdec,APplx,dpmra+Smualpha,dpmdec+Smudelta,Vr0,HIP_EPOCH,GaiaEpoch);
    vector<Type> gaiaresidualAPA(5);
    vector<Type> gaiaresidualAPB(5);

    gaiaresidualAPA[0] = ((gaiatrueAP[0]-GDRAP[0])*cos(gaiatrueAP[1]*DEG2RAD)/MAS2DEG + gaiaresA[0])/GDRAPsig[0];
    gaiaresidualAPA[1] = ((gaiatrueAP[1]-GDRAP[1])/MAS2DEG + gaiaresA[1])/GDRAPsig[1];
    gaiaresidualAPA[2] = (gaiatrueAP[2]-GDRAP[2] + gaiaresA[2])/GDRAPsig[2];
    gaiaresidualAPA[3] = (gaiatrueAP[3]-GDRAP[3] + gaiaresA[3])/GDRAPsig[3];
    gaiaresidualAPA[4] = (gaiatrueAP[4]-GDRAP[4] + gaiaresA[4])/GDRAPsig[4];

    gaiaresidualAPB[0] = ((gaiatrueAP[0]-GDRAPB[0])*cos(gaiatrueAP[1]*DEG2RAD)/MAS2DEG + gaiaresB[0])/GDRAPsigB[0];
    gaiaresidualAPB[1] = ((gaiatrueAP[1]-GDRAPB[1])/MAS2DEG + gaiaresB[1])/GDRAPsigB[1];
    gaiaresidualAPB[2] = (gaiatrueAP[2]-GDRAPB[2] + gaiaresB[2])/GDRAPsigB[2];
    gaiaresidualAPB[3] = (gaiatrueAP[3]-GDRAPB[3] + gaiaresB[3])/GDRAPsigB[3];
    gaiaresidualAPB[4] = (gaiatrueAP[4]-GDRAPB[4] + gaiaresB[4])/GDRAPsigB[4];

    if (solGAP==2) {
      vector<Type> gaiaresidualRADEC(2);
      gaiaresidualRADEC[0] = gaiaresidualAPA[0];
      gaiaresidualRADEC[1] = gaiaresidualAPA[1];
      matrix<Type> RADECcormat(2,2);
      RADECcormat(0,0) = 1;
      RADECcormat(0,1) = GDRcormat(0,1);
      RADECcormat(1,0) = GDRcormat(1,0);
      RADECcormat(1,1) = 1;
      density::MVNORM_t<Type> GDR2neglogdmvnormRADEC(RADECcormat);
      nll += GDR2neglogdmvnormRADEC(gaiaresidualRADEC);
    } else {
      density::MVNORM_t<Type> GDR2neglogdmvnorm(GDRcormat);
      nll += GDR2neglogdmvnorm(gaiaresidualAPA);
    }
    if (solGAPB==2) {
      vector<Type> gaiaresidualRADECB(2);
      gaiaresidualRADECB[0] = gaiaresidualAPB[0];
      gaiaresidualRADECB[1] = gaiaresidualAPB[1];
      matrix<Type> RADECcormatB(2,2);
      RADECcormatB(0,0) = 1;
      RADECcormatB(0,1) = GDRcormatB(0,1);
      RADECcormatB(1,0) = GDRcormatB(1,0);
      RADECcormatB(1,1) = 1;
      density::MVNORM_t<Type> GDR2neglogdmvnormRADEC(RADECcormatB);
      nll += GDR2neglogdmvnormRADEC(gaiaresidualRADECB);
    } else {
      density::MVNORM_t<Type> GDR2neglogdmvnormB(GDRcormatB);
      nll += GDR2neglogdmvnormB(gaiaresidualAPB);
    }
  }
  return nll;
}


