#include <math.h>
#include "sofa.h"

/**
 *  ra, dec             (in deg)
 *  time      double    (in Julian days since J2000)
 *  scanangle double    (in deg)
 *  @return parallax factor in local plane coordinates system: use fw in along scan direction.
 */

DataFrame parallaxfactor(double ra0,double dec0, double time, double scanangle) {
  NumericVector be = getEarthBarycentricPosition(time); // in AU BCRS
  ra0 = ra0 * M_PI/180.;
  dec0 = dec0 * M_PI/180.;
  scanangle = scanangle * M_PI/180.;
  NumericVector p0 = NumericVector::create(-sin(ra0),cos(ra0),0); // local east
  NumericVector q0 = NumericVector::create(-sin(dec0)*cos(ra0),-sin(dec0)*sin(ra0),cos(dec0)); // local north
  double fa = -innerProduct(p0,be);
  double fd = -innerProduct(q0,be);
  double fw = (fa*sin(scanangle) + fd*cos(scanangle)); // al
  double fz = (-fa*cos(scanangle) + fd*sin(scanangle)); // ac
  return(DataFrame::create(_["fw"]=fw,_["fz"]=fz));
}


/**
 * time   double      (in Julian days since J2000)
 */

NumericVector getEarthBarycentricPosition(double time) {
  double ehpv[2][3], ebpv[2][3];
  /* Earth barycentric & heliocentric position/velocity (au, au/d). */
  (void) iauEpv00(DJ00, time, ehpv, ebpv);
  return NumericVector::create(ebpv[0][0],ebpv[0][1],ebpv[0][2]);
}
